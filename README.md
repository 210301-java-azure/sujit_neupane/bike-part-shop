**Bike-Part-Shop Web App (First Iteration Back End)**

**Technologies Used:**
REST, Java, Git, Javalin, Azure, DevOps, SQL, JDBC

We have four main models in this project. They are Part, Associate, Employee, Customer, and Invoice.
- Part: Defines part with its number, name, and retail price
- Associate: It represents Employee and Customer with entities like Full Name, Phone Number, and Email address 
- Employee and Customer: Inherits from Associate
- Invoice: Defines an invoice with its number, invoice date, customer id, employee id, and a map which can hold part names and its quantities.

_PostgreSQL is used as a database._

Operations we can perform:
 Note: All operations require token authentication.
- Part: Adding and deleting part can be performed. We can also query all parts or query part by their part number. Part’s price can be updated.
- Customer: Getting all customers or getting customer based on their full name is possible. Only add is allowed for customer but not delete. We can even update customer’s phone number.
- Employee: Getting all employees or getting employee by their name is possible. Add and delete operations on employee are allowed. We can update employee’s phone number as well.
- Invoice: We can get all invoices and add new invoice to database.


package dev.neu.controllers;

import dev.neu.models.Associate;
import dev.neu.models.Employee;
import dev.neu.service.AssociateService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


public class AssociateControllerTest {
    @InjectMocks
    private AssociateController associateController;

    @Mock
    private AssociateService associateService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleGetAllEmployees() {
        Context context = mock(Context.class);

        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Sam Hit", "2000003000", "sam@gmail.com", true));
        when(associateService.getAllEmployees()).thenReturn(employees);
        associateController.handleGetAllEmployees(context);
        verify(context).json(employees);
    }

    /*
    @Test
    public void testHandleAddAssociate() {
        Context context = mock(Context.class);

        List<Associate> associates = new ArrayList<>();
        Associate associate = new Associate("Sam Hit", "2000003000", "sam@gmail.com", true);
        associates.add(associate);
        when(associateService.addAssociate(associate)).thenReturn(associate);
        associateController.handleAddAssociate(context);
        verify(context).json(associates);
    }

     */
}

package dev.neu.controllers;

import dev.neu.models.Invoice;
import dev.neu.models.Part;
import dev.neu.service.InvoiceService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class InvoiceControllerTest {
    @InjectMocks
    private InvoiceController invoiceController;

    @Mock
    private InvoiceService invoiceService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleGetAllInvoices() {
        Context context = mock(Context.class);
        Part part = new Part(6, "ring", 29.00);
        Map<String, Integer> partMap = new HashMap<>();
        partMap.put(part.getPartName(), 3);
        LocalDateTime time = LocalDateTime.now();
        Invoice newInvoice = new Invoice(4, partMap, time, "Bind Budhathoki", "Puja Budhathoki");

        List<Invoice> invoices = new ArrayList<>();
        invoices.add(newInvoice);

        when(invoiceService.getAllInvoices()).thenReturn(invoices);
        invoiceController.handleGetInvoices(context);
        verify(context).json(invoices);
    }

}

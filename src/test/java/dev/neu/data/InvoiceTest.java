package dev.neu.data;

import dev.neu.data.InvoiceDao;
import dev.neu.data.PartDao;
import dev.neu.models.Invoice;
import dev.neu.models.Part;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InvoiceTest {

    @Test
    public void testAddInvoice() {
        PartDao partDao = new PartDao();
        Part part = partDao.getPartByPartNumber(5);
        Map<String, Integer> partMap = new HashMap<>();
        partMap.put(part.getPartName(), 3);
        LocalDateTime time = LocalDateTime.now();
        Invoice newInvoice = new Invoice(4, partMap, time, "Bind Budhathoki", "Puja Budhathoki");
        InvoiceDao invoiceDao = new InvoiceDao();
        Invoice addedInvoice = invoiceDao.addInvoice(newInvoice);
        Assertions.assertEquals(addedInvoice, newInvoice);
    }
}

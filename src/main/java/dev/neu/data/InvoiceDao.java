package dev.neu.data;

import dev.neu.models.Associate;
import dev.neu.models.Invoice;
import dev.neu.util.ConnectionUtil;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class to manipulate invoice table in database
 */

public class InvoiceDao {

   private Logger logger = LoggerFactory.getLogger(InvoiceDao.class);
   private AssociateDao associateDao = new AssociateDao();
   private PartDao partDao = new PartDao();

    public Invoice addInvoice(Invoice invoice) {
        PreparedStatement preparedStatement;
        // getting customer id based on customer name in invoice
        String customerName = invoice.getCustomerName();
        int customerId = associateDao.getAssociateIdByName(customerName, false);

        // getting employee id based on employee name in invoice
        String employeeName = invoice.getEmployeeName();
        int employeeId = associateDao.getAssociateIdByName(employeeName, true);

        try (Connection connection = ConnectionUtil.getConnection()) {
            // query to insert invoice into db
            preparedStatement = connection.prepareStatement("insert into invoice " +
                    "(invoice_number, invoice_date, customer_id, employee_id) values (?, ?, ?, ?)");
            preparedStatement.setInt(1, invoice.getInvoiceNumber());
            preparedStatement.setObject(2, invoice.getInvoiceDate());
            preparedStatement.setInt(3, customerId);
            preparedStatement.setInt(4, employeeId);
            preparedStatement.executeUpdate();
            logger.info("Invoice with id {} is successfully added to db", invoice.getInvoiceNumber());
        } catch (SQLException e) {
            logException(e);
        }
        // add invoices map with parts and quantities to part_invoice table
        Map<String, Integer> partMap = invoice.getPartWithQuantity();
        addToPartInvoice(partMap, invoice.getInvoiceNumber());
        return invoice;
    }

    private void addToPartInvoice(Map<String, Integer> partMap, int invoiceNumber) {
        for (Map.Entry<String, Integer> part: partMap.entrySet()) {
            String partName = part.getKey();
            int partQuantity = part.getValue();
            int partId = partDao.getPartIdByPartName(partName);
            helperAddToPartInvoice(invoiceNumber, partId, partQuantity);
        }
    }

    private void helperAddToPartInvoice(int invoiceId, int partId, int partQuantity) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into part_invoice" +
                    "(invoice_id, part_id, quantity) values (?, ?, ?)");
            preparedStatement.setInt(1, invoiceId);
            preparedStatement.setInt(2, partId);
            preparedStatement.setInt(3, partQuantity);
            preparedStatement.executeUpdate();
            logger.info("Invoice id {}, part id: {}, part quantity: {} are added to part_invoice table", invoiceId, partId, partQuantity);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public List<Invoice> getAllInvoices() {
        List<Invoice> allInvoices = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from invoice");
            allInvoices = addResultSetToList(resultSet);
        } catch (SQLException e) {
            logException(e);
        }
        return allInvoices;
    }

    public List<Invoice> getInvoicesByAssociate(String name) throws NotFoundResponse {
        List<Invoice> allInvoices = new ArrayList<>();
        Associate associate = associateDao.getAssociateByName(name, true); // need to dealt with customer's related invoices search
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int associateId = 0;
        if (associate.isEmployee()) {
            logger.info("Is Employee");
            associateId = associateDao.getAssociateIdByName(name, true);
        } else {
            logger.info("Is customer");
            associateId = associateDao.getAssociateIdByName(name, false);
            logger.info("Associate Id is {}", associateId);
        }
        try (Connection connection = ConnectionUtil.getConnection()) {
            if (associate.isEmployee()) {
                preparedStatement = connection.prepareStatement("select * from invoice where employee_id = ?");
            } else {
                preparedStatement = connection.prepareStatement("select * from invoice where customer_id = ?");
            }
            preparedStatement.setInt(1, associateId);
            resultSet = preparedStatement.executeQuery();
            allInvoices = addResultSetToList(resultSet);
        } catch (SQLException e) {
            logException(e);
        }
        return allInvoices;
    }

    /**
     * Converts queried result set to invoice object, puts it in a list and returns it
     * @param resultSet queried result set from database
     * @return List of invoices
     * @throws SQLException
     */
    private List<Invoice> addResultSetToList(ResultSet resultSet) throws SQLException {
        List<Invoice> allInvoices = new ArrayList<>();
        while (resultSet.next()) {
            int invoiceNumber = resultSet.getInt("invoice_number");
            LocalDateTime date = resultSet.getDate("invoice_date").toLocalDate().atTime(LocalTime.now());
            String customerName = associateDao.getAssociateNameById(resultSet.getInt("customer_id"), false);
            String employeeName = associateDao.getAssociateNameById(resultSet.getInt("employee_id"), true);
            // customer and employee name can be null, because they are set to null if related employee or customer is deleted from database
            Invoice invoice = new Invoice(invoiceNumber, date, customerName, employeeName);
            allInvoices.add(invoice);
        }
        return allInvoices;
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }
}

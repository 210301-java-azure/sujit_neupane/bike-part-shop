package dev.neu.models;

public class Customer extends Associate {

    public Customer() {
        super();
    }

    public Customer(String fullName, String phoneNumber, String emailAddress) {
        super(fullName, phoneNumber, emailAddress);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress +
                '}';
    }
}

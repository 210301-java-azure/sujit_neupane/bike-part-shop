package dev.neu.models;

import java.util.Objects;

public class Associate {
    protected String fullName;
    protected String phoneNumber;
    protected String emailAddress;
    protected Boolean employee;

    public Associate() {
        super();
    }

    // this constructor is used to instantiate customer object
    public Associate(String fullName, String phoneNumber, String emailAddress) {
        super();
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.employee = false;
    }

    // this constructor is used to instantiate employee object
    public Associate(String fullName, String phoneNumber, String emailAddress, Boolean employee) {
        super();
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.employee = employee;
    }

    public boolean isEmployee() {
        return employee;
    }

    public void set_employee(Boolean employee) {
        employee = employee;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Associate associate = (Associate) o;
        return fullName.equals(associate.fullName) && Objects.equals(phoneNumber, associate.phoneNumber) && Objects.equals(emailAddress, associate.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, phoneNumber, emailAddress);
    }
}

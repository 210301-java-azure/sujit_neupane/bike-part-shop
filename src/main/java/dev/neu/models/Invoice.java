package dev.neu.models;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

public class Invoice {
    private int invoiceNumber;
    // this map contains ordered part name and its quantity
    private Map<String, Integer> partWithQuantity;
    private LocalDateTime invoiceDate;
    private String employeeName;
    private String customerName;

    public Invoice() {
        super();
    }

    public Invoice(int invoiceNumber, LocalDateTime invoiceDate, String employeeName, String customerName) {
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.employeeName = employeeName;
        this.customerName = customerName;
    }

    public Invoice(int invoiceNumber, Map<String, Integer> partWithQuantity, LocalDateTime invoiceDate, String employeeName, String customerName) {
        this.invoiceNumber = invoiceNumber;
        this.partWithQuantity = partWithQuantity;
        this.invoiceDate = invoiceDate;
        this.employeeName = employeeName;
        this.customerName = customerName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Map<String, Integer> getPartWithQuantity() {
        return partWithQuantity;
    }

    public void setPartWithQuantity(Map<String, Integer> partWithQuantity) {
        this.partWithQuantity = partWithQuantity;
    }

    public LocalDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return invoiceNumber == invoice.invoiceNumber && invoiceDate.equals(invoice.invoiceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceNumber, invoiceDate);
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceNumber=" + invoiceNumber +
                ", \npartWithQuantity=" + partWithQuantity +
                ", \ninvoiceDate=" + invoiceDate +
                ", employeeName='" + employeeName + '\'' +
                ", customerName='" + customerName + '\'' +
                '}';
    }
}

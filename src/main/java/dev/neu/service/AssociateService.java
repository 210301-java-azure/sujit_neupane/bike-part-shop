package dev.neu.service;

import dev.neu.data.AssociateDao;
import dev.neu.models.Associate;
import dev.neu.models.Customer;
import dev.neu.models.Employee;

import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AssociateService {

    private AssociateDao associateDao = new AssociateDao();
    private Logger logger = LoggerFactory.getLogger(AssociateService.class);

    public Associate addAssociate(Associate associate) {
        return associateDao.addAssociate(associate);
    }

    public List<Customer> getAllCustomers() {
        return associateDao.getAllCustomers();
    }

    public List<Employee> getAllEmployees() {
        return associateDao.getAllEmployees();
    }

    public Associate getAssociateByName(String fullName, boolean employee) {
        return associateDao.getAssociateByName(fullName, employee);
    }

    public void deleteEmployee(String fullName) {
        associateDao.deleteEmployee(fullName);
    }

    public void updatePhoneNumber(String phoneNumber, String associateName,boolean employee) {
        if (associateDao.getAssociateIdByName(associateName, employee) == 0) {
            logger.info("Associate {} is not on record", associateName);
            throw new NotFoundResponse("Associate " + associateName + " is not on record");
        }
        associateDao.updatePhoneNumber(phoneNumber, associateName, employee);
    }
}

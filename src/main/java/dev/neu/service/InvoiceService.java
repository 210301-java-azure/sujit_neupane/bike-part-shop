package dev.neu.service;

import dev.neu.data.InvoiceDao;
import dev.neu.models.Invoice;

import java.util.List;

public class InvoiceService {

    private InvoiceDao invoiceDao = new InvoiceDao();

    public Invoice addInvoice(Invoice invoice) {
        return invoiceDao.addInvoice(invoice);
    }

    public List<Invoice> getInvoicesByAssociate(String name) {
        return invoiceDao.getInvoicesByAssociate(name);
    }

    public List<Invoice> getAllInvoices() {
        return invoiceDao.getAllInvoices();
    }
}

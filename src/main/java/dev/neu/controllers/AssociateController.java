package dev.neu.controllers;

import dev.neu.models.Associate;
import dev.neu.service.AssociateService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for Associates
 * Implements handle method of Handler interface
 * get, delete, put, post operations are performed
 */

public class AssociateController {

    private Logger logger = LoggerFactory.getLogger(AssociateController.class);
    private AssociateService associateService = new AssociateService();

    public void handleAddAssociate(Context ctx) {
        Associate associate = ctx.bodyAsClass(Associate.class);
        logger.info("Adding associate {}", associate.getFullName());
        associateService.addAssociate(associate);
        ctx.status(201);
    }

    public void handleGetAllCustomers(Context ctx) {
        logger.info("Getting all customers");
        ctx.json(associateService.getAllCustomers());
    }

    public void handleGetAllEmployees(Context ctx) {
        logger.info("Getting all employees");
        ctx.json(associateService.getAllEmployees());
    }

    public void handleDeleteEmployee(Context ctx) {
        String fullName = ctx.queryParam("fullName");
        logger.info("Deleting employee {}", fullName);
        associateService.deleteEmployee(fullName);
    }

    public void handleUpdateEmployeePhoneNumber(Context ctx) {
        String employeeName = ctx.queryParam("employeeName");
        String phoneNumber = ctx.queryParam("phoneNumber");
        logger.info("Updating employee {}'s phone number", employeeName);
        associateService.updatePhoneNumber(phoneNumber, employeeName, true);
    }

    public void handleUpdateCustomerPhoneNumber(Context ctx) {
        String customerName = ctx.queryParam("customerName");
        String phoneNumber = ctx.queryParam("phoneNumber");
        logger.info("Updating customer {}'s phone number", customerName);
        associateService.updatePhoneNumber(phoneNumber, customerName, false);
    }

    public void handleGetCustomerByName(Context ctx) {
        String fullName = ctx.pathParam("fullName");
        Associate associate = associateService.getAssociateByName(fullName, false);
        if (associate == null) {
            logger.warn("Customer {} is not found", fullName);
            throw new NotFoundResponse("Customer \"" + fullName + "\" is not found");
        } else {
            logger.info("Getting customer {}'s info", fullName);
            ctx.json(associate);
        }
    }

    public void handleGetEmployeeByName(Context ctx) {
        String fullName = ctx.pathParam("fullName");
        Associate associate = associateService.getAssociateByName(fullName, true);
        if (associate == null) {
            logger.warn("Employee {} is not found", fullName);
            throw new NotFoundResponse("Employee \"" + fullName + "\" is not found");
        } else {
            logger.info("Getting employee {}'s info", fullName);
            ctx.json(associate);
        }
    }
}

package dev.neu.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx) {
        String userName = ctx.formParam("username");
        String password = ctx.formParam("password");
        logger.info("{} attempted login", userName);
        if (userName != null && userName.equals("sujit")) {
            if (password != null && password.equals("secret")) {
                logger.info("Successful login by {}", userName);
                ctx.header("Authorization", "token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Incorrect username or password");
        }
        throw new UnauthorizedResponse("Incorrect username or password");
    }

    public void authorizeToken(Context ctx) {
        logger.info("Authorizing token");

        String authHeader = ctx.header("Authorization");

        if (authHeader != null && authHeader.equals("token")) {
            logger.info("Login is authorized");
        } else {
            logger.warn("Authorization failed");
            throw new UnauthorizedResponse();
        }
    }
}

package dev.neu;

import dev.neu.controllers.AssociateController;
import dev.neu.controllers.AuthController;
import dev.neu.controllers.InvoiceController;
import dev.neu.controllers.PartController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

/**
 * Bike-part-shop web app
 * We can perform operations like add, delete, and query for parts, employees, customers, and invoices
 * @version 03/18/2021
 * @author Sujit Neupane
 */

public class BikePartShopApp {

    private PartController partController = new PartController();
    private AssociateController associateController = new AssociateController();
    private InvoiceController invoiceController = new InvoiceController();
    private AuthController authController = new AuthController();

    Javalin app = Javalin.create().routes(()->{
        path("parts", ()->{
            before("/",authController::authorizeToken);
            get(partController::handleGetAllParts);
            post(partController::handleAddPart);
            delete(partController::handleDeletePartByName);
            path(":partNumber", ()->{
                before("/",authController::authorizeToken);
                get(partController::handleGetPartByPartNumber);
                put(partController::handleUpdateRetailPrice);
            });
        });
        path("customers", ()->{
            before("/",authController::authorizeToken);
            get(associateController::handleGetAllCustomers);
            post(associateController::handleAddAssociate);
            put(associateController::handleUpdateCustomerPhoneNumber);
            path(":fullName", ()->{
                before("/",authController::authorizeToken);
                get(associateController::handleGetCustomerByName);
            });
        });
        path("employees", ()->{
            before("/",authController::authorizeToken);
            get(associateController::handleGetAllEmployees);
            post(associateController::handleAddAssociate);
            put(associateController::handleUpdateEmployeePhoneNumber);
            delete(associateController::handleDeleteEmployee);
            path(":fullName", ()->{
                before("/",authController::authorizeToken);
                get(associateController::handleGetEmployeeByName);
            });
        });
        path("invoices", ()->{
            before("/",authController::authorizeToken);
            get(invoiceController::handleGetInvoices);
            post(invoiceController::handleAddInvoice);
        });
        path("login", ()->
            post(authController::authenticateLogin)
        );
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
